/******************************************************************************
 * ISerialTest.ino
 * Copyright (c) 2022 Thomas Kerr AB3GY
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief Example and test sketch for the ISerial class.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <Arduino.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "ISerial.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/
#define SERIAL_BAUD (115200)
#define SERIAL_BUF_LEN (128)


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

// Global ISerial object for testing.
// Choose a serial UART that you can put in loopback mode by placing a jumper
// between TXD and RXD.
ISerial my_serial;
 
/******************************************************************************
 * Local data.
 ******************************************************************************/
static const char* str_test1 = "TEST STRING ABCDEFGHIJKLMNOPQRSTUVWXYZ 0123456789!";
static char serial_buf[SERIAL_BUF_LEN];


/******************************************************************************
 * Public functions.
 ******************************************************************************/


/**************************************
 * flush_read()
 **************************************/
void flush_read()
{
    while (my_serial.available() > 0)
    {
        my_serial.readBytes(serial_buf, my_serial.available());
    }
}


/**************************************
 * setup()
 **************************************/ 
void setup()
{
    // Turn the Arduino built-in LED off.
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
    
    // Arduino serial monitor init.
    Serial.begin(SERIAL_BAUD);
    delay(2000);
    Serial.println("ISerial test sketch.");
    Serial.println("Place ISerial port in loopback mode using a jumper between TXD and RXD.");
    
    // Initialize the ISerial test object.
    my_serial.setSerial(&Serial1);
    if (my_serial.begin(SERIAL_BAUD))
    {
        Serial.println("Test serial port initialization successful.");
    }
    else
    {
        Serial.println("Test serial port initialization FAILED.");
    }
    
    // Check if test object is ready.
    if (my_serial)
    {
        Serial.println("Test serial port is ready.");
    }
    else
    {
        Serial.println("Test serial port NOT READY.");
    }
    
    // Check serial buffer space available for writing.
    Serial.print("Serial buffer space available for writing: ");
    Serial.print(my_serial.availableForWrite());
    Serial.println(" bytes");
    
    // Send some loopback data using print().
    int str_len = strlen(str_test1);
    Serial.print("Sending loopback data 1... sent: "); Serial.print(str_len);
    my_serial.print(str_test1);
    delay(100);
    int num_avail = my_serial.available();
    Serial.print(" available: "); Serial.print(num_avail);
    int num_read = my_serial.readBytes(serial_buf, num_avail);
    Serial.print(" read: "); Serial.print(num_read);
    Serial.println();
    flush_read();
    
    // Send some loopback data using write().
    Serial.print("Sending loopback data 2... sent: "); Serial.print(str_len);
    my_serial.write(str_test1, str_len);
    delay(100);
    num_avail = my_serial.available();
    Serial.print(" available: "); Serial.print(num_avail);
    num_read = my_serial.readBytes(serial_buf, num_avail);
    Serial.print(" read: "); Serial.print(num_read);
    Serial.println();
    flush_read();
    
    // Test peek() and read().
    Serial.print("Testing peek() and read(): ");
    my_serial.print(str_test1);
    delay(100);
    while (my_serial.available() > 0)
    {
        int p = my_serial.peek();
        int r = my_serial.read();
        Serial.print((char)p);
        if (p != r)
        {
            Serial.print("\nERROR: '");
            Serial.print((char)p); Serial.print("' does not match '");
            Serial.print((char)r); Serial.println("'");
        }
    }
    Serial.println();
    flush_read();
    
    // Test flush().
    Serial.print("Testing flush(): ");
    my_serial.print(str_test1);
    my_serial.flush();
    if (my_serial.available() == str_len)
    {
        Serial.print("passed.");
    }
    else
    {
        Serial.print("FAILED.");
    }
    Serial.println();
    flush_read();

    // Test the find() method.
    Serial.print("Finding string 1... ");
    my_serial.write(str_test1, str_len);
    delay(100);
    if (my_serial.find("STRING"))
    {
        Serial.print("found. ");
    }
    else
    {
        Serial.print("NOT FOUND. ");
    }
    Serial.println();
    
    Serial.print("Finding string 2... ");
    if (my_serial.find("12345", 5))
    {
        Serial.print("found. ");
    }
    else
    {
        Serial.print("NOT FOUND. ");
    }
    Serial.println();
    flush_read();

    // Test parseInt().
    Serial.print("Testing parseInt(): ");
    my_serial.print("-9874.0625 ABCDE");
    delay(100);
    long x = my_serial.parseInt();
    Serial.println(x);
    flush_read();
    
    // Test parseFloat().
    Serial.print("Testing parseFloat(): ");
    my_serial.print("-9874.0625 ABCDE");
    delay(100);
    float f = my_serial.parseFloat();
    Serial.println(f, 4);
    flush_read();

    Serial.println("Done.");
}


/**************************************
 * loop()
 **************************************/ 
void loop()
{

}


/******************************************************************************
 * Private functions.
 ******************************************************************************/

// End of file.