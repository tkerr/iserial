# ISerial #
Arduino Serial UART interface class.

Allows other classes to use the Arduino Serial UART without having to know which serial port is in use. Also provides hardware architecture independence for the user class.

### Author ###
Tom Kerr AB3GY

### License ###
Released under the MIT License  
https://opensource.org/licenses/MIT
