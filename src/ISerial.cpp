/******************************************************************************
 * ISerial.cpp
 * Copyright (c) 2019 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * Arduino Serial UART interface class.
 *
 * Allows other classes to use the Arduino Serial UART without having to know 
 * which serial port is in use.
 * 
 * Also provides hardware architecture independence for the user class.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include "Arduino.h"


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "ISerial.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/


/******************************************************************************
 * Local data.
 ******************************************************************************/


/******************************************************************************
 * Public methods and functions.
 ******************************************************************************/

/**************************************
 * ISerial::ISerial
 **************************************/
ISerial::ISerial() :
    m_serial_class(SERIAL_UNKNOWN),
    m_pSerial(0),
    m_init(false)
{
    // Nothing else to do.
}

/**************************************
 * ISerial::ISerial
 **************************************/
ISerial::ISerial(HardwareSerial* pSerial) :
    m_serial_class(SERIAL_HARDWARE),
    m_init(true)
{
    // The serial classes all derive from Stream.
    m_pSerial = dynamic_cast<Stream*>(pSerial);
}

/**************************************
 * ISerial::setSerial
 **************************************/
void ISerial::setSerial(HardwareSerial* pSerial)
{
    m_serial_class = SERIAL_HARDWARE;
    m_init = true;
    
    // The serial classes all derive from Stream.
    m_pSerial = dynamic_cast<Stream*>(pSerial);
}


#ifdef ARDUINO_ARCH_SAMD
/**************************************
 * ISerial::ISerial
 **************************************/
ISerial::ISerial(Serial_* pSerial) :
    m_serial_class(SERIAL_SAMD_USB),
    m_init(true)
{
    // The serial classes all derive from Stream.
    m_pSerial = dynamic_cast<Stream*>(pSerial);
}

/**************************************
 * ISerial::setSerial
 **************************************/
void ISerial::setSerial(Serial_* pSerial)
{
    m_serial_class = SERIAL_SAMD_USB;
    m_init = true;
    
    // The serial classes all derive from Stream.
    m_pSerial = dynamic_cast<Stream*>(pSerial);
}
#endif // ARDUINO_ARCH_SAMD


#ifdef ARDUINO_ARCH_TEENSY
/**************************************
 * ISerial::ISerial
 **************************************/
ISerial::ISerial(usb_serial_class* pSerial) :
    m_serial_class(SERIAL_TEENSY_USB),
    m_init(true)
{
    // The serial classes all derive from Stream.
    m_pSerial = dynamic_cast<Stream*>(pSerial);
}

/**************************************
 * ISerial::setSerial
 **************************************/
void ISerial::setSerial(usb_serial_class* pSerial)
{
    m_serial_class = SERIAL_TEENSY_USB;
    m_init = true;
    
    // The serial classes all derive from Stream.
    m_pSerial = dynamic_cast<Stream*>(pSerial);
}
#endif // ARDUINO_ARCH_TEENSY


/**************************************
 * ISerial::begin
 **************************************/
bool ISerial::begin(unsigned long baud, unsigned int format)
{
    bool ok = false;
    
    if (m_init)
    {
        // The begin() method is not part of the Stream class.
        // We must call the begin() method on the original object class.
        switch (m_serial_class)
        {
        case SERIAL_SAMD_USB:
            #ifdef ARDUINO_ARCH_SAMD
            {
                Serial_* p = static_cast<Serial_*>(m_pSerial);
                p->begin(baud, format);
                ok = true;
            }
            #endif
            break;
            
        case SERIAL_TEENSY_USB:
            #ifdef ARDUINO_ARCH_TEENSY
            {
                usb_serial_class* p = static_cast<usb_serial_class*>(m_pSerial);
                p->begin(baud);
                ok = true;
            }
            #endif
            break;
            
        case SERIAL_HARDWARE:
            {
                HardwareSerial* p = static_cast<HardwareSerial*>(m_pSerial);
                p->begin(baud, format);
                ok = true;
            }
            break;
            
        default:
            break;
        }
    }
    return ok;
}

/**************************************
 * ISerial::end
 **************************************/
void ISerial::end()
{
    if (m_init)
    {
        // The end() method is not part of the Stream class.
        // We must call the end() method on the original object class.
        switch (m_serial_class)
        {
        case SERIAL_SAMD_USB:
            #ifdef ARDUINO_ARCH_SAMD
            {
                Serial_* p = static_cast<Serial_*>(m_pSerial);
                p->end();
            }
            #endif
            break;
            
        case SERIAL_TEENSY_USB:
            #ifdef ARDUINO_ARCH_TEENSY
            {
                usb_serial_class* p = static_cast<usb_serial_class*>(m_pSerial);
                p->end();
            }
            #endif
            break;
            
        case SERIAL_HARDWARE:
            {
                HardwareSerial* p = static_cast<HardwareSerial*>(m_pSerial);
                p->end();
            }
            break;
            
        default:
            break;
        }
    }
}

/**************************************
 * ISerial::operator bool
 **************************************/
ISerial::operator bool()
{
    if (m_init)
    {
        // The bool operator is not part of the Stream class.
        // We must call the bool operator method on the original object class.
        switch (m_serial_class)
        {
        case SERIAL_SAMD_USB:
            #ifdef ARDUINO_ARCH_SAMD
            {
                Serial_* p = static_cast<Serial_*>(m_pSerial);
                return p->operator bool();
            }
            #endif
            break;
            
        case SERIAL_TEENSY_USB:
            #ifdef ARDUINO_ARCH_TEENSY
            {
                usb_serial_class* p = static_cast<usb_serial_class*>(m_pSerial);
                return p->operator bool();
            }
            #endif
            break;
            
        case SERIAL_HARDWARE:
            {
                HardwareSerial* p = static_cast<HardwareSerial*>(m_pSerial);
                return p->operator bool();
            }
            break;
            
        default:
            break;
        }
    }
    return false;
}

/**************************************
 * ISerial::find
 **************************************/
bool ISerial::find(const char *target)
{
    if (!m_init) return false;
    
    // Adafruit SAMD package has a different signature in Stream.h.
    // Add additional boards as needed.
    #if defined(ADAFRUIT_FEATHER_M0) || defined(ADAFRUIT_FEATHER_M0_EXPRESS) || defined(ADAFRUIT_METRO_M0_EXPRESS)
    return m_pSerial->find((char*)target);
    #else
    return m_pSerial->find(target);
    #endif
}

/**************************************
 * ISerial::find
 **************************************/
bool ISerial::find(const uint8_t *target)
{
    if (!m_init) return false;
    
    // Adafruit SAMD package has a different signature in Stream.h.
    // Add additional boards as needed.
    #if defined(ADAFRUIT_FEATHER_M0) || defined(ADAFRUIT_FEATHER_M0_EXPRESS) || defined(ADAFRUIT_METRO_M0_EXPRESS)
    return m_pSerial->find((uint8_t*)target);
    #else
    return m_pSerial->find(target);
    #endif
}

/**************************************
 * ISerial::find
 **************************************/
bool ISerial::find(const char *target, size_t length)
{
    if (!m_init) return false;
    
    // Adafruit SAMD package has a different signature in Stream.h.
    // Add additional boards as needed.
    #if defined(ADAFRUIT_FEATHER_M0) || defined(ADAFRUIT_FEATHER_M0_EXPRESS) || defined(ADAFRUIT_METRO_M0_EXPRESS)
    return m_pSerial->find((char*)target, length);
    #else
    return m_pSerial->find(target, length);
    #endif
}

/**************************************
 * ISerial::find
 **************************************/
bool ISerial::find(const uint8_t *target, size_t length)
{
    if (!m_init) return false;
    
    // Adafruit SAMD package has a different signature in Stream.h.
    // Add additional boards as needed.
    #if defined(ADAFRUIT_FEATHER_M0) || defined(ADAFRUIT_FEATHER_M0_EXPRESS) || defined(ADAFRUIT_METRO_M0_EXPRESS)
    return m_pSerial->find((uint8_t*)target, length);
    #else
    return m_pSerial->find(target, length);
    #endif
}

/**************************************
 * ISerial::findUntil
 **************************************/
bool ISerial::findUntil(const char *target, const char *terminator)
{
    if (!m_init) return false;
    
    // Adafruit SAMD package has a different signature in Stream.h.
    // Add additional boards as needed.
    #if defined(ADAFRUIT_FEATHER_M0) || defined(ADAFRUIT_FEATHER_M0_EXPRESS) || defined(ADAFRUIT_METRO_M0_EXPRESS)
    return m_pSerial->findUntil((char*)target, (char*)terminator);
    #else
    return m_pSerial->findUntil(target, terminator);
    #endif
}

/**************************************
 * ISerial::findUntil
 **************************************/
bool ISerial::findUntil(const uint8_t *target, const char *terminator)
{
    if (!m_init) return false;
    
    // Adafruit SAMD package has a different signature in Stream.h.
    // Add additional boards as needed.
    #if defined(ADAFRUIT_FEATHER_M0) || defined(ADAFRUIT_FEATHER_M0_EXPRESS) || defined(ADAFRUIT_METRO_M0_EXPRESS)
    return m_pSerial->findUntil((uint8_t*)target, (char*)terminator);
    #else
    return m_pSerial->findUntil(target, terminator);
    #endif
}


/*****************************************************************************
 * Protected methods and functions.
 ******************************************************************************/


/*****************************************************************************
 * Private methods and functions.
 ******************************************************************************/
 

// End of file.
