/*****************************************************************************
 * ISerial.h
 * Copyright (c) 2022 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ****************************************************************************/

/**
 * @file
 * @brief
 * Arduino Serial UART interface class.
 *
 * Allows other classes to use the Arduino Serial UART without having to know 
 * which serial port is in use.
 * 
 * Also provides hardware architecture independence for the user class.
 */
#ifndef _ISERIAL_H_
#define _ISERIAL_H_

/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <stdbool.h>
#include "Arduino.h"
#include "Print.h"
#include "IPAddress.h"


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "arduino_arch.h"


/******************************************************************************
 * Public definitions.
 ******************************************************************************/
 
 
/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

 
/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/
 
/**
 * @class ISerial
 * @brief Arduino Serial UART interface class.
 *
 * Allows other classes to use the Arduino Serial UART without having to know 
 * which serial port is in use.
 * 
 * Also provides hardware architecture independence for the user class.
 *
 * See the 'Serial' function reference in the Arduino documentation:
 *   - https://www.arduino.cc/reference/en/language/functions/communication/serial
 * 
 */
class ISerial
{
public:

    /****
     * The ISerial object can be default constructed or initialized with a pointer 
     * to the serial port object as an argument.  If default constructed, the the
     * serial port must be assigned with setSerial().
     *
     * Different processor architectures provide different classes for the
     * Serial object.  Constructors are provided for the different classes.
     *
     * Note that the serial device must be initialized before it can be used.
     * This can be done by calling its begin() method directly, or by calling
     * the ISerial::begin() method.
     *
     * Limitations:
     *   1) serialEvent() is not supported.
     ****/

    ISerial();  //!< Default constructor
    
    ISerial(HardwareSerial* pSerial);         //!< Construct from HardwareSerial and its subclasses
    void setSerial(HardwareSerial* pSerial);  //!< Set the serial port if default constructed
    
    #ifdef ARDUINO_ARCH_SAMD
    ISerial(Serial_* pSerial);         //!< SAMD USB serial device
    void setSerial(Serial_* pSerial);  //!< Set the serial port if default constructed
    #endif  // ARDUINO_ARCH_SAMD
    
    #ifdef ARDUINO_ARCH_TEENSY
    ISerial(usb_serial_class* pSerial);         //!< Teensy USB serial device
    void setSerial(usb_serial_class* pSerial);  //!< Set the serial port if default constructed
    #endif  // ARDUINO_ARCH_TEENSY
    
    /// Call the Serial object's begin method.
    /// Returns true if successful, false otherwise.
    bool begin(unsigned long baud, unsigned int format = SERIAL_8N1);
    void end(void);
    
    operator bool();  // Provides if(Serial) functionality
    
    int available(void) {return (m_init)? m_pSerial->available() : 0;}
    int availableForWrite(void)  {return (m_init)? m_pSerial->availableForWrite() : 0;}

    bool find(const char *target);
    bool find(const uint8_t *target);
    bool find(const char *target, size_t length);
    bool find(const uint8_t *target, size_t length);
    bool find(char target) {return m_pSerial->find(target);}
    
    bool findUntil(const char *target, const char *terminator);
    bool findUntil(const uint8_t *target, const char *terminator);
    
    void flush(void) {if (m_init) m_pSerial->flush();}
    
    #ifdef ARDUINO_ARCH_TEENSY
    // Teensyduino has a different signature for these methods than mainstream Arduino.
    long parseInt() {return (m_init)? m_pSerial->parseInt() : 0;}
	long parseInt(char skipChar) {return (m_init)? m_pSerial->parseInt(skipChar) : 0;}
	float parseFloat() {return (m_init)? m_pSerial->parseFloat() : 0.0f;}
	float parseFloat(char skipChar) {return (m_init)? m_pSerial->parseFloat(skipChar) : 0.0f;}
    #else
    long parseInt(LookaheadMode lookahead = SKIP_ALL, char ignore = '\x01')
        {return (m_init)? m_pSerial->parseInt(lookahead, ignore) : 0;}
    float parseFloat(LookaheadMode lookahead = SKIP_ALL, char ignore = '\x01')
        {return (m_init)? m_pSerial->parseFloat(lookahead, ignore) : 0.0f;}
    #endif
    
    int peek(void) {return (m_init)? m_pSerial->peek() : 0;}
    int read(void) {return (m_init)? m_pSerial->read() : 0;}
    
    size_t readBytes(char *buffer, size_t length) 
        {return (m_init)? m_pSerial->readBytes(buffer, length) : 0;}
    size_t readBytes(uint8_t *buffer, size_t length) 
        {return (m_init)? m_pSerial->readBytes(buffer, length) : 0;}
    
    size_t readBytesUntil(char terminator, char *buffer, size_t length)
        {return (m_init)? m_pSerial->readBytesUntil(terminator, buffer, length) : 0;}
    size_t readBytesUntil(char terminator, uint8_t *buffer, size_t length)
        {return (m_init)? m_pSerial->readBytesUntil(terminator, buffer, length) : 0;}
        
    String readString() {return (m_init)? m_pSerial->readString() : String();}
    String readStringUntil(char terminator) 
        {return (m_init)? m_pSerial->readStringUntil(terminator) : String();}
    
    void setTimeout(unsigned long timeout) {if (m_init) m_pSerial->setTimeout(timeout);}
    
    size_t write(uint8_t val) {return (m_init)? m_pSerial->write(val) : 0;}
    size_t write(char val) {return (m_init)? m_pSerial->write((uint8_t)val) : 0;}
    size_t write(int val) {return (m_init)? m_pSerial->write((uint8_t)val) : 0;}
    size_t write(long val) {return (m_init)? m_pSerial->write((uint8_t)val) : 0;}
    size_t write(unsigned int val) {return (m_init)? m_pSerial->write((uint8_t)val) : 0;}
    size_t write(unsigned long val) {return (m_init)? m_pSerial->write((uint8_t)val) : 0;}
    size_t write(uint8_t* buffer, size_t length) 
        {return (m_init)? m_pSerial->write(buffer, length) : 0;}
    size_t write(const uint8_t* buffer, size_t length) 
        {return (m_init)? m_pSerial->write((uint8_t*)buffer, length) : 0;}
    size_t write(char* buffer, size_t length) 
        {return (m_init)? m_pSerial->write((uint8_t*)buffer, length) : 0;}
    size_t write(const char* buffer, size_t length) 
        {return (m_init)? m_pSerial->write((uint8_t*)buffer, length) : 0;}
    
    // All of the print() variants.
    void print(const String &s) {if (m_init) m_pSerial->print(s);}
    void print(char c) {if (m_init) m_pSerial->print(c);}
    void print(const char s[]) {if (m_init) m_pSerial->print(s);}
    void print(const __FlashStringHelper *f) {if (m_init) m_pSerial->print(f);}
    
    void print(unsigned char n, int base = DEC) {if (m_init) m_pSerial->print(n, base);}
    void print(int n, int base = DEC) {if (m_init) m_pSerial->print(n, base);}
    void print(unsigned int n, int base = DEC) {if (m_init) m_pSerial->print(n, base);}
    void print(long n, int base = DEC){if (m_init) m_pSerial->print(n, base);}
    void print(unsigned long n, int base = DEC) {if (m_init) m_pSerial->print(n, base);}
    
    void print(double n, int digits = 2) {if (m_init) m_pSerial->print(n, digits);}
    void print(const Printable &obj) {if (m_init) m_pSerial->print(obj);}
    
    // All of the println() variants.
    void println(void) {if (m_init) m_pSerial->println();}
    void println(const String &s) {if (m_init) m_pSerial->println(s);}
    void println(char c) {if (m_init) m_pSerial->println(c);}
    void println(const char s[]) {if (m_init) m_pSerial->println(s);}
    void println(const __FlashStringHelper *f) {if (m_init) m_pSerial->println(f);}
    
    void println(unsigned char n, int base = DEC) {if (m_init) m_pSerial->println(n, base);}
    void println(int n, int base = DEC) {if (m_init) m_pSerial->println(n, base);}
    void println(unsigned int n, int base = DEC) {if (m_init) m_pSerial->println(n, base);}
    void println(long n, int base = DEC) {if (m_init) m_pSerial->println(n, base);}
    void println(unsigned long n, int base = DEC) {if (m_init) m_pSerial->println(n, base);}
    
    void println(double n, int digits = 2) {if (m_init) m_pSerial->println(n, digits);}
    void println(const Printable &obj) {if (m_init) m_pSerial->println(obj);}

protected:
    
private:

    /**
     * @brief Enumerate the serial port class used for this object
     */
    enum SerialClass
    {
        SERIAL_UNKNOWN = 0,   //!< Unknown/unsupported class
        SERIAL_HARDWARE,      //!< HardwareSerial class
        SERIAL_SAMD_USB,      //!< SAMD Serial_ class
        SERIAL_TEENSY_USB,    //!< Teensy usb_serial_class
    };
        
    SerialClass m_serial_class;  //!< The serial port class in use
    Stream*     m_pSerial;       //!< Pointer to the serial port object
    bool        m_init;          //!< True if object has been initialized
};


#endif // _ISERIAL_H_
